<?php
include("header.php");
include("ConnectBDD.php");?>
<link href="recherche.css"rel="stylesheet" >
    <form action='recherche.php' method="POST" id="barrerecherche">
      Rechercher le(s) film(s) par
      <select name='rechercheCriteres'>
          <option value='nom'>Nom</option>
          <option value='realisateur'>Realisateur</option>
          <option value='pays'>Pays</option>
      </select>
      <input type="text" name="rechercheText">
      <input type="submit" name="rechercheFilm">
      <br> Trier les films par:
      <input type='radio' name='criteres' value='titre' id='criteres-1'>
      <label for='criteres-1'>Titre</label>
      <input type='radio' name='criteres' value='alphabetique' id='criteres-2'>
      <label for='criteres-2'>Ordre alphabétique</label>
      <input type='radio' name='criteres' value='pays' id='criteres-3'>
      <label for='criteres-3'>Pays</label>
      <input type='radio' name='criteres' value='chronologique' id='criteres-4'>
      <label for='criteres-4'>Ordre chronologique</label>
      <input type='radio' name='criteres' value='duree' id='criteres-5'>
      <label for='criteres-5'>Durée</label>
      <input type='radio' name='criteres' value='realisateur' id='criteres-6'>
      <label for='criteres-6'>Réalisateur</label>
      <br>
      <input type="submit" name="criteresFilm">

    </form>
  </body>
</html>
<?php
if (isset($_POST['rechercheFilm'])) {

    if ($_POST['rechercheCriteres'] == 'nom') {
        $result = $bdd->query("SELECT * FROM films WHERE titre_original LIKE '" . $_POST['rechercheText'] . "%'");
        echo "<ul>";

        foreach ($result as $c) {
            echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
        }
        echo "</ul>";
    }

    elseif ($_POST['rechercheCriteres'] == 'pays') {
        $result = $bdd->query("SELECT * FROM films WHERE pays LIKE '" . $_POST['rechercheText'] . "%'");
        echo "<ul>";

        foreach ($result as $c) {
            echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
        }
        echo "</ul>";
    } elseif ($_POST['rechercheCriteres'] == 'realisateur') {
        $result = $bdd->query("SELECT * FROM films WHERE realisateur LIKE '" . $_POST['rechercheText'] . "%'");
        echo "<ul>";

        foreach ($result as $c) {
            echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
        }
        echo "</ul>";
    }
}

elseif (isset($_POST['criteresFilm'])) {
    if (isset($_POST['criteres'])) {
        if ($_POST['criteres'] == 'titre') {
            $result = $bdd->query("SELECT * from films");
            echo "<ul>";

            foreach ($result as $c) {
                echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
            }
            echo "</ul>";

        } elseif ($_POST['criteres'] == 'alphabetique') {
            $result = $bdd->query("SELECT * from films ORDER BY titre_original");
            echo "<ul>";
            #fecc00
            foreach ($result as $c) {
                echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
            }
            echo "</ul>";

        } elseif ($_POST['criteres'] == 'pays') {
            $result = $bdd->query("SELECT DISTINCT pays from films ORDER BY pays");
            echo "<ul>";

            foreach ($result as $c) {
                echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
            }
            echo "</ul>";
        } elseif ($_POST['criteres'] == 'chronologique') {
            $result = $bdd->query("SELECT * from films ORDER BY date");
            echo "<ul>";

            foreach ($result as $c) {
                echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
            }
            echo "</ul>";
        } elseif ($_POST['criteres'] == 'duree') {
            $result = $bdd->query("SELECT * from films ORDER BY duree");
            echo "<ul>";

            foreach ($result as $c) {
                echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
            }
            echo "</ul>";
        } elseif ($_POST['criteres'] == 'realisateur') {
            $result = $bdd->query("SELECT DISTINCT realisateur from films ORDER BY realisateur");
            echo "<ul>";

            foreach ($result as $c) {
                echo "<li> <a href=\"infos.php?id=".$c['code_film']."\">".$c['titre_original']."</a> </li>";
            }
            echo "</ul>";
        }

        else {
            echo "NON";
        }
    }
}
