<?php
try {
    $bdd = new PDO('sqlite:./dbfilms');
    $bdd->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);
}
catch (PDOException $e) {
    print "Erreur : " . $e->getMessage() . "<br/>";
}
?>
